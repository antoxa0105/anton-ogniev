# Hi, I'm Anton 👋
I'm a responsible person, purposeful, attentive to details. My experience has made me resistant to stress, taught me to plan every little thing, stay focused and be best friends while doing multiple tasks at the same time. For the past 12 years I have been working in international companies and I believe that my experience will be useful on my future in 
IT field.

<p align='center'>
   <a href="https://www.linkedin.com/in/anton-ohniev-6b7b37b6">
       <img src="https://img.shields.io/badge/linkedin-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white"/>
   </a>
<p align='center'>
   📫 How to reach me: <a href='mailto:antoxa0105@gmail.com'>antoxa0105@gmail.com</a>
</p>

### NON-IT experience
<p>
   <a href="https://danone.ua/#danone">
       <img width="100" src="https://designprint.com.ua/wp-content/uploads/2020/01/danon-logo02-client-designprintcomua.png"/>
   </a>

   <a href="http://pepsico.ua/">
       <img width="100" src="https://logospng.org/download/pepsico/logo-pepsico-256.png"/>
   </a>
   <a href="https://lactalis.com.ua/">
       <img width="100" src="https://www.business-solutions-atlantic-france.com/wp-content/uploads/2019/06/logo-lactalis.png"/>
   </a>
</p>
 Procurement professional with over 10 years experience in  FMCG,
  thereof 3 years of team management.
  Good understanding of business processes, procurement operations. Developed communication and leadership skills, experience in managing  communication with stakeholders.

## 🛠 Technical Stack
* Java Script, HTML/CSS
* React.js, React.js, Hooks, Redux
* Basick nowledge Node.js
* Understanding of OOP
* Responsive Web Design
* Preprcessor SCSS (SASS)
* Gulp
* Versions control systems (Git)
* Figma

## 🙋 Personal Skills
* English: pre-intermediate
* Result-oriented, proactive team player, fast learner
* Strong analytic skills
* Good communication skills
* Problem solving and trouble shooting skills

